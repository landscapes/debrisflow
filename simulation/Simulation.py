import random
import numpy as np
import torch
import torchvision
import tqdm
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'


class Simulation:
    '''
    Simulation class

    __init__       : Initialize simulation variables
    run            : Run the simulation
    calc_slope     : Calculate slope
    calc_edge      : Calculate receiver cells
    calc_drainarea : Calculate discharge
    calc_spl       : Fluvial processes
    calc_df        : Debris-flow processes
    calc_hs        : Hillslope processes
    calc_lake      : Lakes
    '''

    from simulation.misc       import calc_slope, calc_edge, calc_drainarea
    from simulation.fluvial    import calc_spl
    from simulation.debrisflow import calc_df
    from simulation.hillslope  import calc_hs
    from simulation.lake       import calc_lake

    def __init__(self, height, dx, seed):
        '''
        Initialize simulation variables

        Parameters:
            height : Initial terrain
            dx     : Cell size (in meters)
            seed   : Random seed
        '''

        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)

        self.res = height.shape[-1]
        self.dx  = dx

        # Bounds
        self.boundary        = torch.ones(1, self.res, self.res, device=device)
        self.boundary[0,0,:] = self.boundary[0,-1,:] = self.boundary[0,:,0] = self.boundary[0,:,-1] = 0.0
        
        # Layers
        self.layer_b = torch.tensor(height, device=device).reshape(1, self.res, self.res)   # Bedrock layer
        self.layer_d = torch.zeros(1, self.res, self.res, device=device) * self.boundary    # Debris layer
        self.layer_r = torch.zeros(1, self.res, self.res, device=device) * self.boundary    # Regolith layer
        self.layer_s = torch.zeros(1, self.res, self.res, device=device) * self.boundary    # Sediment layer
        self.dem = self.layer_b + self.layer_d + self.layer_r + self.layer_s
        
        # Uplift
        self.uplift_mask = (self.layer_b - self.layer_b.min()) / (self.layer_b.max() - self.layer_b.min()) + 0.5

        # Slope
        self.eqn_slope = torch.zeros(1, self.res, self.res, device=device)
        self.slope     = torch.zeros(4, self.res, self.res, device=device)
        
        # Random tensor for single random receiver
        self.rcv_rand  = torch.rand(self.slope.shape, device=device)
        
        # Flows
        self.drainarea = torch.zeros(1, self.res, self.res, device=device)      # Water discharge
        self.flow_d    = torch.zeros(1, self.res, self.res, device=device)      # Flow rate of debris
        self.flow_s    = torch.zeros(1, self.res, self.res, device=device)      # Flow rate of sediments

        # Lake parameters
        self.blur            = torchvision.transforms.GaussianBlur(9, sigma=8.0)
        self.lakeflow_flag   = False
        self.lake            = torch.zeros(1, self.res, self.res, device=device)
        self.lake_surface    = torch.zeros(1, self.res, self.res, device=device)    # Lake surface for rendering
        self.lake_surface_id = torch.zeros(1, self.res, self.res, device=device)
        self.lake_drain      = torch.zeros(1, self.res, self.res, device=device)    # Drain at sill points
        self.lake_drain_id   = torch.zeros(1, self.res, self.res, device=device)
        self.lake_seed_id    = torch.arange(self.res**2, device=device) + 1.0       # To seed new lakes

        # Variables for deposition
        self.dtt        = torch.zeros(1, self.res, self.res, device=device) * self.boundary
        self.hn         = torch.zeros(4, self.res, self.res, device=device) * self.boundary
        self.fd         = torch.zeros(4, self.res, self.res, device=device) * self.boundary
        self.donor_mask = torch.zeros(4, self.res, self.res, device=device) * self.boundary
        self.h_rcv      = torch.zeros(1, self.res, self.res, device=device) * self.boundary
        self.h_rcv1     = torch.zeros(1, self.res, self.res, device=device) * self.boundary
        self.h_rcv2     = torch.zeros(1, self.res, self.res, device=device) * self.boundary
        self.Q          = torch.zeros(1, self.res, self.res, device=device) * self.boundary


    def run(self, niter=3000, dt=128, k_uplift=0.0006, k_rain=0.00049,
                  ke_spl=0.0007, kd_spl=1.0, spl_mn=0.4,    # Fluvial
                  ke_df=0.001, kt_df=0.3, ks_df=0.005, slope_df=0.57, debris_mn=0.08, debris_mnt=0.25,  # Debris-flow
                  ke_hs=0.04,   # Hillslope
                  lake_flag=1):
        '''
        Run the simulation

        Parameters:
            niter      : Number of iteration of simulation
            dt         : Timestep
            k_uplift   : Uplift rate
            k_rain     : Precipitation constant
            ke_spl     : Fluvial erosion
            kd_spl     : Fluvial deposition
            spl_mn     : Fluvial exponent (m/n ratio)
            ke_df      : Debris-flow erosion
            kt_df      : Debris-flow threshold
            ks_df      : Thermal erosion
            slope_df   : Thermal critical angle
            debris_mn  : Debris-flow exponent (alpha/beta ratio)
            debris_mnt : Debris-flow threshold exponent (alpha_t/beta_t ratio)
            ke_hs      : Hillslope constant
            lake_flag  : Process lake toggle
        '''

        hb_rcv = torch.zeros(1, self.res, self.res, device=device)
        rain = self.dx**2 * k_rain
        self.drainarea[0,0,:] = self.drainarea[0,-1,:] = self.drainarea[0,:,0] = self.drainarea[0,:,-1] = rain

        for _ in tqdm.tqdm(range(niter)):
            self.calc_slope(self.dem, blur_flag=True)
            edge_sr = self.calc_edge('single_random')
            edge_h  = self.calc_edge('hybrid')

            self.calc_lake(edge_sr)
            self.calc_drainarea(rain, edge_sr, lake_flag)
            self.calc_slope(self.dem, blur_flag=False)

            hb_rcv[0,1:-1,1:-1] = torch.minimum(torch.minimum(self.layer_b[0,2:,1:-1], self.layer_b[0,:-2,1:-1]), torch.minimum(self.layer_b[0,1:-1,2:], self.layer_b[0,1:-1,:-2]))

            # Equations 5, 6
            self.layer_b += dt * k_uplift * self.uplift_mask    # Uplift
            self.calc_hs(ke_hs, dt)
            self.calc_spl(ke_spl, kd_spl, spl_mn, dt, edge_h)
            self.calc_df(ke_df, kt_df, ks_df, slope_df, debris_mn, debris_mnt, dt, edge_h)

            self.layer_b = torch.maximum(self.layer_b, hb_rcv)
            self.layer_b[0,:,0]  = torch.maximum(2*self.layer_b[0,:,1]  - self.layer_b[0,:,2] , self.layer_b[0,:,1] )
            self.layer_b[0,:,-1] = torch.maximum(2*self.layer_b[0,:,-2] - self.layer_b[0,:,-3], self.layer_b[0,:,-2])
            self.layer_b[0,0,:]  = torch.maximum(2*self.layer_b[0,1,:]  - self.layer_b[0,2,:] , self.layer_b[0,1,:] )
            self.layer_b[0,-1,:] = torch.maximum(2*self.layer_b[0,-2,:] - self.layer_b[0,-3,:], self.layer_b[0,-2,:])
            self.layer_b = self.layer_b - self.layer_b.min()

            self.dem = self.layer_b + self.layer_d + self.layer_r + self.layer_s

        self.calc_slope(self.dem, blur_flag=True)
        edge_sr = self.calc_edge('single_random')
        self.calc_lake(edge_sr, lake_iter=100, flow_iter=100, sill_update_iter=100)

