import torch.nn.functional as F


def calc_hs(self, ke_hs, dt):
    '''
    Hillslope processes (Equation 4)

    Parameters:
        ke_hs: Hillslope constant
    '''
    # Combined deposit layer
    layer_c = self.layer_r + self.layer_d + self.layer_s
    height = self.layer_b + layer_c

    dhdt = F.pad( -4 *  height[0,1:-1,1:-1] + \
                        height[0,2:,1:-1]   + \
                        height[0,:-2,1:-1]  + \
                        height[0,1:-1,2:]   + \
                        height[0,1:-1,:-2], (1,1,1,1)) * \
                        ke_hs / self.dx**2
    
    # Erode layer_c with negative part of dh/dt 
    new_layer_c  = layer_c + dhdt.clamp(max = 0) * dt
    # Erode the bedrock with the negative part of layer_c
    self.layer_b += new_layer_c.clamp(max = 0)

    # Update eroded deposition layers
    remaining_layer_ratio = (new_layer_c.clamp(min=0)) / (layer_c + (layer_c == 0))
    self.layer_r *= remaining_layer_ratio
    self.layer_d *= remaining_layer_ratio
    self.layer_s *= remaining_layer_ratio

    # Add positive part of dh/dt to the regolith layer
    self.layer_r += dhdt.clamp(min = 0) * dt

