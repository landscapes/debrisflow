import torch
import torch.nn.functional as F


def calc_df(self, ke_df, kt_df, ks_df, slope_df, debris_mn, debris_mnt, dt, edge):
    '''
    Debris-flow processes (Equation 7)

    Parameters:
        ke_df      : Debris-flow erosion
        kt_df      : Debris-flow threshold
        ks_df      : Thermal erosion
        slope_df   : Thermal critical angle
        debris_mn  : Debris-flow exponent (alpha/beta ratio)
        debris_mnt : Debris-flow threshold exponent (alpha_t/beta_t ratio)
    '''
    # Combined deposit layer
    layer_c = self.layer_r + self.layer_d + self.layer_s

    # Equation 11
    self.Q[0,1:-1,1:-1] = self.dx**2 * \
                          (edge[0,2:,1:-1]  * self.flow_d[0,2:,1:-1]  + \
                           edge[1,:-2,1:-1] * self.flow_d[0,:-2,1:-1] + \
                           edge[2,1:-1,2:]  * self.flow_d[0,1:-1,2:]  + \
                           edge[3,1:-1,:-2] * self.flow_d[0,1:-1,:-2])        

    # Erosion
    M  = self.eqn_slope * torch.pow(self.Q, debris_mn)
    Mt = self.eqn_slope * torch.pow(self.Q, debris_mnt)
    threshold = torch.where(Mt>0.0, 1.0 - kt_df / Mt, 0.0)  # Equation 13
    rate_e = ke_df * F.relu(threshold) * M + ks_df * F.relu(self.eqn_slope - slope_df)  # Erosion rate (Equation 12, 8)

    # Deposition
    h = self.layer_b + self.layer_d + self.layer_r + self.layer_s   # Terrain elevation
    h_ = h * 1.0

    self.dtt = dt   # Spatially filled dt
    self.dtt *= self.boundary
    
    # Height of neighbour of cell
    self.hn[0,1:-1,1:-1] = h[0,2:,1:-1]
    self.hn[1,1:-1,1:-1] = h[0,:-2,1:-1]
    self.hn[2,1:-1,1:-1] = h[0,1:-1,2:]
    self.hn[3,1:-1,1:-1] = h[0,1:-1,:-2]
    self.hn *= self.boundary
    
    # Debris flux
    self.fd[0,1:-1,1:-1] = edge[0,2:,1:-1]  * self.flow_d[0,2:,1:-1]
    self.fd[1,1:-1,1:-1] = edge[1,:-2,1:-1] * self.flow_d[0,:-2,1:-1]
    self.fd[2,1:-1,1:-1] = edge[2,1:-1,2:]  * self.flow_d[0,1:-1,2:]
    self.fd[3,1:-1,1:-1] = edge[3,1:-1,:-2] * self.flow_d[0,1:-1,:-2]
    self.fd *= self.boundary
    
    # Store 4-neighboring donors
    self.donor_mask[0,1:-1,1:-1] = edge[0,2:,1:-1]
    self.donor_mask[1,1:-1,1:-1] = edge[1,:-2,1:-1]
    self.donor_mask[2,1:-1,1:-1] = edge[2,1:-1,2:]
    self.donor_mask[3,1:-1,1:-1] = edge[3,1:-1,:-2]
    self.donor_mask *= self.boundary

    bias = torch.randperm(4)    # Since torch.min() tie-breaks in ascending order
    for _ in range(4):  # Iterate through the 4-neighbors
        # Find lowest elevation donor not yet processed
        hd, po = torch.min(torch.where(self.donor_mask>1e-3, self.hn, 1e8)[bias,:,:], dim=0, keepdim=True)
        pos = torch.zeros_like(po)
        pos[po == 0] = bias[0]
        pos[po == 1] = bias[1]
        pos[po == 2] = bias[2]
        pos[po == 3] = bias[3]

        sum_fd = self.fd.sum(dim=0)
        td = F.relu(torch.where(sum_fd > 0.0, (hd - h) / sum_fd, 0.0))
        h += torch.minimum(td, self.dtt) * sum_fd   # Add deposit to height

        # Remove processed donor
        self.donor_mask.scatter_(0, pos, 0.0)
        self.fd.scatter_(0, pos, 0.0)
        self.dtt = F.relu(self.dtt-td)

    h = torch.minimum(h, self.hn.max(dim=0)[0])
    
    # Limit height based on debris flow condition with receiver
    self.h_rcv[0,1:-1,1:-1] = edge[0,1:-1:,1:-1] * h_[0,:-2,1:-1] + \
                              edge[1,1:-1:,1:-1] * h_[0,2:,1:-1]  + \
                              edge[2,1:-1:,1:-1] * h_[0,1:-1,:-2] + \
                              edge[3,1:-1:,1:-1] * h_[0,1:-1,2:]
    
    # Max height to avoid df condition (Equation 20)
    self.h_rcv1[0,1:-1,1:-1] = torch.minimum(h_[0,1:-1,:-2], h_[0,1:-1,2:])
    self.h_rcv2[0,1:-1,1:-1] = torch.minimum(h_[0,2:,1:-1], h_[0,:-2,1:-1])
    self.h_rcv1[self.h_rcv1 > h_] = 0.0
    self.h_rcv2[self.h_rcv2 > h_] = 0.0
    S = (kt_df / (torch.pow(self.Q, debris_mnt) + (self.Q==0.0))).clamp(max=slope_df)
    sqterm = 8 * self.dx**2 * S**2 - 4 * (self.h_rcv1 - self.h_rcv2)**2
    sqterm[sqterm < 0.0] = 0.0
    h_max = (self.h_rcv1 + self.h_rcv2) / 2.0 + 0.25 * torch.sqrt(sqterm)
    h_max_sr = torch.where(self.Q > 0.0, self.h_rcv + self.dx * S, h_)
    h_max[sqterm == 0.0] = h_max_sr[sqterm == 0.0]

    h = torch.minimum(h, h_max)
    rate_d = F.relu(h - h_) / dt    # Deposition rate
    dhdt = torch.where(self.lake_surface > 0.0, rate_d, rate_d - rate_e)

    # Erode layer_c with negative part of dh/dt
    new_layer_c  = layer_c + dhdt.clamp(max = 0) * dt
    # Erode the bedrock with the negative part of layer_c
    self.layer_b += new_layer_c.clamp(max = 0)

    # Update eroded deposition layers
    remaining_layer_ratio = (new_layer_c.clamp(min=0)) / (layer_c + (layer_c == 0))
    self.layer_r *= remaining_layer_ratio
    self.layer_d *= remaining_layer_ratio
    self.layer_s *= remaining_layer_ratio

    # Add positive part of dh/dt to the debris layer
    self.layer_d += dhdt.clamp(min = 0) * dt

    # Update debris flux
    self.flow_d[0,1:-1,1:-1] =  (-dhdt)[0,1:-1,1:-1] + \
                                edge[0,2:,1:-1]  * self.flow_d[0,2:,1:-1]  + \
                                edge[1,:-2,1:-1] * self.flow_d[0,:-2,1:-1] + \
                                edge[2,1:-1,2:]  * self.flow_d[0,1:-1,2:]  + \
                                edge[3,1:-1,:-2] * self.flow_d[0,1:-1,:-2]
    self.flow_d = F.relu(self.flow_d)

