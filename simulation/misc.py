import torch
import torch.nn.functional as F


def calc_slope(self, dem, blur_flag=False):
    '''
    Calculate slope (Equation 18)
    '''
    if blur_flag:
        dem = self.blur(dem)

    self.slope[0,1:,:]  = dem[0,1:,:]  - dem[0,:-1,:]   # up
    self.slope[1,:-1,:] = dem[0,:-1,:] - dem[0,1:,:]    # down
    self.slope[2,:,1:]  = dem[0,:,1:]  - dem[0,:,:-1]   # left
    self.slope[3,:,:-1] = dem[0,:,:-1] - dem[0,:,1:]    # right

    self.slope = F.relu(self.slope) / self.dx
    self.eqn_slope = (torch.maximum(self.slope[0], self.slope[1])**2 + torch.maximum(self.slope[2], self.slope[3])**2).sqrt()


def calc_edge(self, receiver_mode='single_random'):
    '''
    Calculate receiver cells
    '''
    if receiver_mode == 'single_random':
        edge = self.slope * self.rcv_rand
        edge = F.softmax(edge * 1e14, dim=0)
        edge = F.relu((edge - 0.5) * 2)
    elif receiver_mode == 'hybrid':
        # Single random receiver
        edge_sr = self.slope * self.rcv_rand
        edge_sr = F.softmax(edge_sr * 1e14, dim=0)
        edge_sr = F.relu((edge_sr - 0.5) * 2)
        # Multiple receiver
        edge_m = torch.pow(self.slope, 0.1) * 1e4
        edge_m = F.normalize(edge_m, p=1, dim=0)

        edge = torch.where((self.eqn_slope < 0.01) | (self.lake_surface>0.0), edge_m, edge_sr)
    else:
        raise NotImplementedError('Receiver mode is not implemented')
    return edge

    
def calc_drainarea(self, rain, edge, lake_flag, approx_iters=1):
    '''
    Calculate discharge (Equation 2, 17)
    '''
    for _ in range(approx_iters):
        self.drainarea[0,1:-1,1:-1] = rain + \
                    lake_flag        * self.lake_drain[0,1:-1,1:-1] + \
                    edge[0,2:,1:-1]  * self.drainarea[0,2:,1:-1]    + \
                    edge[1,:-2,1:-1] * self.drainarea[0,:-2,1:-1]   + \
                    edge[2,1:-1,2:]  * self.drainarea[0,1:-1,2:]    + \
                    edge[3,1:-1,:-2] * self.drainarea[0,1:-1,:-2]

