import torch
import torch.nn.functional as F


def calc_spl(self, ke_spl, kd_spl, spl_mn, dt, edge):
    '''
    Fluvial processes (Equation 1)

    Parameters:
        ke_spl : Fluvial erosion
        kd_spl : Fluvial deposition
        spl_mn : Fluvial exponent (m/n ratio)
    '''
    
    # Combined deposit layer
    layer_c = self.layer_r + self.layer_d + self.layer_s

    # Erosion
    rate_e = ke_spl * torch.pow(self.drainarea, spl_mn) * self.eqn_slope    # First term of Equation 1

    # Deposition
    h = self.layer_b + self.layer_d + self.layer_r + self.layer_s     # Terrain elevation
    h_ = h * 1.0

    self.dtt = dt   # Spatially filled dt
    self.dtt *= self.boundary
    
    # Height of neighbour of cell
    self.hn[0,1:-1,1:-1] = h[0,2:,1:-1]
    self.hn[1,1:-1,1:-1] = h[0,:-2,1:-1]
    self.hn[2,1:-1,1:-1] = h[0,1:-1,2:]
    self.hn[3,1:-1,1:-1] = h[0,1:-1,:-2]
    self.hn *= self.boundary
    
    # Sediment flux
    self.fd[0,1:-1,1:-1] = edge[0,2:,1:-1]  * self.flow_s[0,2:,1:-1]
    self.fd[1,1:-1,1:-1] = edge[1,:-2,1:-1] * self.flow_s[0,:-2,1:-1]
    self.fd[2,1:-1,1:-1] = edge[2,1:-1,2:]  * self.flow_s[0,1:-1,2:]
    self.fd[3,1:-1,1:-1] = edge[3,1:-1,:-2] * self.flow_s[0,1:-1,:-2]
    self.fd *= self.boundary

    # Store 4-neighboring donors
    self.donor_mask[0,1:-1,1:-1] = edge[0,2:,1:-1]
    self.donor_mask[1,1:-1,1:-1] = edge[1,:-2,1:-1]
    self.donor_mask[2,1:-1,1:-1] = edge[2,1:-1,2:]
    self.donor_mask[3,1:-1,1:-1] = edge[3,1:-1,:-2]
    self.donor_mask *= self.boundary

    flow_mult = (kd_spl / self.drainarea * self.dx**2/1024**2).clamp(0.0, 1.0)
    bias = torch.randperm(4)    # Since torch.min() tie-breaks in ascending order
    for _ in range(4):  # Iterate through the 4-neighbors
        # Find lowest elevation donor not yet processed
        hd, po = torch.min(torch.where(self.donor_mask>1e-3, self.hn, 1e8)[bias,:,:], dim=0, keepdim=True)
        pos = torch.zeros_like(po)
        pos[po == 0] = bias[0]
        pos[po == 1] = bias[1]
        pos[po == 2] = bias[2]
        pos[po == 3] = bias[3]

        sum_fd = self.fd.sum(dim=0)
        spl_dep = flow_mult * sum_fd    # Second term of Equation 1
        td = F.relu(torch.where(sum_fd > 0.0, (hd - h) / spl_dep, 0.0))
        h += torch.minimum(td, self.dtt) * spl_dep  # Add deposit to height
        
        # Remove processed donor
        self.donor_mask.scatter_(0, pos, 0.0)
        self.fd.scatter_(0, pos, 0.0)
        self.dtt = F.relu(self.dtt-td)
    
    h = torch.minimum(h, self.hn.max(dim=0)[0])
    rate_d = F.relu(h - h_) / dt    # Deposition rate
    dhdt = torch.where(self.lake_surface > 0.0, rate_d, rate_d - rate_e)

    # Erode layer_c with negative part of dh/dt
    new_layer_c  = layer_c + dhdt.clamp(max = 0) * dt
    # Erode the bedrock with the negative part of layer_c
    self.layer_b += new_layer_c.clamp(max = 0)

    # Update eroded deposition layers
    remaining_layer_ratio = (new_layer_c.clamp(min=0)) / (layer_c + (layer_c == 0))
    self.layer_r *= remaining_layer_ratio
    self.layer_d *= remaining_layer_ratio
    self.layer_s *= remaining_layer_ratio

    # Add positive part of dh/dt to the sediment layer
    self.layer_s += dhdt.clamp(min = 0) * dt

    # Update sediment flux
    self.flow_s[0,1:-1,1:-1] =  (-dhdt)[0,1:-1,1:-1] + \
                                edge[0,2:,1:-1]  * self.flow_s[0,2:,1:-1]  + \
                                edge[1,:-2,1:-1] * self.flow_s[0,:-2,1:-1] + \
                                edge[2,1:-1,2:]  * self.flow_s[0,1:-1,2:]  + \
                                edge[3,1:-1,:-2] * self.flow_s[0,1:-1,:-2]
    self.flow_s = F.relu(self.flow_s)

