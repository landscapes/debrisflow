import random
import torch
import torch.nn.functional as F
from torch_scatter import scatter_min, scatter_sum

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'


def calc_lake(self, edge, split_threshold=8192, lake_iter=1, flow_iter=1, sill_update_iter=1):
    '''
    Lakes

    Parameters:
        edge: works only with single receiver variants
        lake_iter: number of iterations for basin estimation (Figure 17)
        flow_iter: number of iterations for flow estimation (Figure 4)
        sill_update_iter: number of iteration to correct sill after basin estimation
    '''

    # BASIN IDENTIFICATION (Section 6.1)

    local_minima = (torch.sum(edge, 0) == 0.0) & self.boundary.to(torch.bool) 

    # Split basins
    lm = torch.where(local_minima, self.lake, 0.0)
    output, counts = torch.unique(lm[lm>0], sorted=False, return_counts=True)
    output = output[counts>1]
    if output.size(0) > 0:
        idx = torch.where(lm == output[random.sample(range(output.size(0)), 1)])
        i, j = random.sample(range(idx[0].size(0)), 2)
        r1, c1, r2, c2 = idx[1][i], idx[2][i], idx[1][j], idx[2][j]
        distance = torch.sqrt((r1-r2)**2 + (c1-c2)**2)*self.dx
        if distance > split_threshold:
            if self.drainarea[0,r1,c1] < self.drainarea[0,r2,c2]:
                self.lake[0,r1,c1] = 0.0
            else:
                self.lake[0,r2,c2] = 0.0

    # Seed local minimas        
    p = torch.where((local_minima) & (self.lake == 0))
    self.lake[p] = self.lake_seed_id[:p[0].size(0)] + self.lake.max()
    self.lake = self.lake * self.boundary
    
    # Propogate seed
    local_minima_value = torch.where(local_minima, self.lake, 0.0)
    for _ in range(lake_iter):
        self.lake[0,1:-1,1:-1] = edge[0,1:-1:,1:-1] * self.lake[0,:-2,1:-1] + \
                                 edge[1,1:-1:,1:-1] * self.lake[0,2:,1:-1]  + \
                                 edge[2,1:-1:,1:-1] * self.lake[0,1:-1,:-2] + \
                                 edge[3,1:-1:,1:-1] * self.lake[0,1:-1,2:]
        self.lake = torch.where(local_minima, local_minima_value, self.lake) * self.boundary
    self.lake = torch.round(self.lake)

    # If any sill comes in its own basin, then update the sills
    i0, i1, i2 = torch.where((self.lake > 0.0) & (self.lake == self.lake_drain_id))
    ldrainid = self.lake_drain_id[i0,i1,i2]
    ldrain = self.lake_drain[i0,i1,i2]
    self.lake_drain_id[i0,i1,i2] = 0.0
    self.lake_drain[i0,i1,i2] = 0.0
    self.drainarea[i0,i1,i2] -= ldrain
    
    # If it has a donor, go sill_update_iter steps towards it
    for _ in range(sill_update_iter):
        i1 = torch.where(edge[0, (i1+1).clamp(1,self.res-2), i2] > 1e-1, (i1+1).clamp(1,self.res-2), i1)
        i1 = torch.where(edge[1, (i1-1).clamp(1,self.res-2), i2] > 1e-1, (i1-1).clamp(1,self.res-2), i1)
        i2 = torch.where(edge[2, i1, (i2+1).clamp(1,self.res-2)] > 1e-1, (i2+1).clamp(1,self.res-2), i2)
        i2 = torch.where(edge[3, i1, (i2-1).clamp(1,self.res-2)] > 1e-1, (i2-1).clamp(1,self.res-2), i2)
    
    # Once at lake edge, relocate the sill to another basin if found, else remove it
    i1 = torch.where((self.lake[i0,(i1+1).clamp(1,self.res-2),i2] != ldrainid), (i1+1).clamp(1,self.res-2), i1)
    i1 = torch.where((self.lake[i0,(i1-1).clamp(1,self.res-2),i2] != ldrainid), (i1-1).clamp(1,self.res-2), i1)
    i2 = torch.where((self.lake[i0,i1,(i2+1).clamp(1,self.res-2)] != ldrainid), (i2+1).clamp(1,self.res-2), i2)
    i2 = torch.where((self.lake[i0,i1,(i2-1).clamp(1,self.res-2)] != ldrainid), (i2-1).clamp(1,self.res-2), i2)
    self.lake_drain[i0,i1,i2] += torch.where((self.lake[i0,i1,i2] != ldrainid), ldrain, 0.0)
    self.lake_drain_id[i0,i1,i2] = torch.where((self.lake[i0,i1,i2] != ldrainid), ldrainid, 0.0)


    # COMPUTATION OF DEPRESSION OUTFLOW (Section 6.2)

    if self.lakeflow_flag==False and torch.any(local_minima):
        self.lakeflow_flag = True

        # Recenter seed values to avoid them getting too big
        lake_id_sub = self.lake[self.lake > 0].min() - 1
        self.lake[self.lake > 0]                       -= lake_id_sub
        self.lake_drain_id[self.lake_drain_id > 0]     -= lake_id_sub
        self.lake_surface_id[self.lake_surface_id > 0] -= lake_id_sub
        local_minima_value[local_minima_value > 0]     -= lake_id_sub

        basin_min = torch.unique(local_minima_value, sorted=False)  # Valid lakes
        basin_min = basin_min[basin_min > 0]
        self.lake_left = basin_min

        # Remove lakes that were processed in the past and don't exist anymore
        idx = torch.where(~torch.isin(self.lake_drain_id, basin_min))
        self.lake_drain[idx]    = 0.0
        self.lake_drain_id[idx] = 0.0
        idx = torch.where(~torch.isin(self.lake_surface_id, basin_min))
        self.lake_surface[idx]    = 0.0
        self.lake_surface_id[idx] = 0.0
        
        
    while flow_iter>0 and self.lakeflow_flag==True:
        flow_iter -= 1

        # Remove invalid lakes
        basin_min = torch.unique(local_minima_value, sorted=False)
        idx = torch.isin(self.lake_left, basin_min)
        remove = self.lake_left[~idx]
        self.lake_left = self.lake_left[idx]
        
        # Remove lakes that would have been processed in the future and don't exist anymore
        idx = torch.isin(self.lake_drain_id, remove)
        self.lake_drain_id[idx] = 0.0
        self.lake_drain[idx]    = 0.0
        idx = torch.isin(self.lake_surface_id, remove)
        self.lake_surface_id[idx] = 0.0
        self.lake_surface[idx]    = 0.0

        if self.lake_left.size(0) == 0:
            self.lakeflow_flag = False
            break

        lake_t = torch.where(torch.isin(self.lake, self.lake_left), self.lake, 0.0)
        
        # Find edges of all lakes
        lake_edge = torch.where(self.boundary==0.0, 1e8, 0.0)
        lake_edge[0,1:-1,1:-1] = torch.where((lake_t[0,1:-1,1:-1] > 0.0) & \
                                            ((lake_t[0,:-2,1:-1] != lake_t[0,1:-1,1:-1]) | \
                                             (lake_t[0,2:,1:-1]  != lake_t[0,1:-1,1:-1]) | \
                                             (lake_t[0,1:-1,:-2] != lake_t[0,1:-1,1:-1]) | \
                                             (lake_t[0,1:-1,2:]  != lake_t[0,1:-1,1:-1])),
                                              self.dem[0,1:-1,1:-1], 1e8)

        # Find min of all lakes edges/potential outflow
        min_h, p = scatter_min(lake_edge.flatten(), lake_t.flatten().long())
        p = p[p < self.res**2][1:]
        px, py = p//self.res, p%self.res

        # Select outflow points that border the solved/no lake parts
        q = torch.where((lake_t[(0,px+1,py)]==0) | (lake_t[(0,px-1,py)]==0) | (lake_t[(0,px,py+1)]==0) | (lake_t[(0,px,py-1)]==0))
        px, py = px[q], py[q]

        # If no such points are found, then follow the same procedure on super-lakes/lake clusters
        if px.size(0) == 0:
            lake_edge = torch.where(self.boundary==0.0, 1e8, 0.0)
            lake_edge[0,1:-1,1:-1] = torch.where((lake_t[0,1:-1,1:-1] > 0.0) & \
                                                ((lake_t[0,:-2,1:-1] == 0.0) | \
                                                 (lake_t[0,2:,1:-1]  == 0.0) | \
                                                 (lake_t[0,1:-1,:-2] == 0.0) | \
                                                 (lake_t[0,1:-1,2:]  == 0.0)),
                                                  self.dem[0,1:-1,1:-1], 1e8)
            p = torch.argmin(lake_edge)
            px, py = p//self.res, p%self.res

        # Lakes that will be processed in this iteration
        p_id = lake_t[(0,px,py)].long()

        # Find drain leaving the lakes
        sill_discharge = scatter_sum(torch.where(torch.isin(lake_t, p_id) & local_minima, self.drainarea, 0.0).flatten(), lake_t.flatten().long())[p_id]

        # Find sill point positions on grid
        pending = torch.full(size=p_id.size(), fill_value=True, dtype=torch.bool, device=device)
        sill_px, sill_py = px*1, py*1
        idx = pending & (lake_t[(0,px+1,py)] == 0.0)
        sill_px[idx] += 1
        pending[idx] = False
        idx = pending & (lake_t[(0,px-1,py)] == 0.0)
        sill_px[idx] -= 1
        pending[idx] = False
        idx = pending & (lake_t[(0,px,py+1)] == 0.0)
        sill_py[idx] += 1
        pending[idx] = False
        idx = pending & (lake_t[(0,px,py-1)] == 0.0)
        sill_py[idx] -= 1
        pending[idx] = False
        assert (~pending).all()


        # DISCHARGE AND LAKE UPDATE (Section 6.3)

        # Update/create new lake drain values, this is used in calc_drainarea function
        idx = torch.isin(self.lake_drain_id, p_id)
        self.lake_drain_id[idx] = 0.0
        self.lake_drain[idx]    = 0.0
        self.lake_drain_id[(0, sill_px, sill_py)] = p_id.float()
        self.lake_drain[(0, sill_px, sill_py)] = sill_discharge

        # Update/create new lake surface, this is used for rendering lakes
        idx = torch.isin(self.lake_surface_id, p_id)
        self.lake_surface_id[idx] = 0.0
        self.lake_surface[idx]    = 0.0
        self.lake_surface_id = torch.where(torch.isin(lake_t, p_id), lake_t, self.lake_surface_id)

        # Get height of the new lakes, which should equal height of the sill surface
        sill_height1d = (self.dem[(0, sill_px, sill_py)] + self.lake_surface[(0, sill_px, sill_py)]).reshape(-1)
        
        # Put sill height at the new lake positions
        sill_height2d = lake_t * 1.0
        idx0, idx1, idx2, idx3 = torch.nonzero(sill_height2d.unsqueeze(3) == p_id, as_tuple=True)
        sill_height2d[(idx0,idx1,idx2)] = sill_height1d[idx3]
        self.lake_surface = torch.where(torch.isin(lake_t, p_id), F.relu(sill_height2d-self.dem), self.lake_surface)

        # Remove processed lakes from to process list and reset flag if list is empty
        self.lake_left = self.lake_left[~torch.isin(self.lake_left, p_id)]
        if self.lake_left.size(0) == 0:
            self.lakeflow_flag = False

        lakett = self.lake * 1.0
        lakett[0,1:-1,1:-1]    = edge[0,1:-1:,1:-1] * self.lake[0,:-2,1:-1] + \
                                 edge[1,1:-1:,1:-1] * self.lake[0,2:,1:-1]  + \
                                 edge[2,1:-1:,1:-1] * self.lake[0,1:-1,:-2] + \
                                 edge[3,1:-1:,1:-1] * self.lake[0,1:-1,2:]
        self.lake_drain[lakett ==  self.lake_drain_id] = 0.0
