# Efficient Debris-flow Simulation for Steep Terrain Erosion

### Authors:
Aryamaan Jain, Bedrich Benes, Guillaume Cordonnier

[Project page](https://www-sop.inria.fr/reves/Basilic/2024/JBC24/) | [Paper](https://www-sop.inria.fr/reves/Basilic/2024/JBC24/2024_Siggraph_Debris_Flow_Author_Version.pdf)

---

## Getting Started

### Prerequisites

Ensure you have [conda](https://docs.anaconda.com/miniconda/) installed on your system. If using GPU acceleration, verify driver [compatibility](https://docs.nvidia.com/deploy/cuda-compatibility/) with CUDA 12.x.

### Installation

1. **Clone the repository:**
    ```sh
    git clone https://gitlab.inria.fr/landscapes/debrisflow.git
    cd debrisflow
    ```

2. **Setup the environment:**
    ```sh
    conda env create -f environment.yml
    conda activate debrisflow
    ```

### Running the Simulation

1. **Execute the simulation:**
    ```sh
    python main.py
    ```

2. **Configuration:**
    - Default simulation parameters can be adjusted in `main.py`.
    - The initial terrain data is placed in `data/init.npy`.
    - Simulation results will be saved in the `data/output/` directory.

---

## File Structure

```
debrisflow/
├── data/                   # Directory for input data and output results
│   ├── init.npy            # Initial terrain data
│   └── output/             # Simulation results
├── simulation/             # Directory containing the simulation code
├── environment.yml         # Conda environment configuration file
├── LICENSE.md              # Project license file
├── main.py                 # Main script to run the simulation
└── README.md               # Project README file
```

---

## BibTeX

```
@article{Jain2024,
  title     = {Efficient Debris-flow Simulation for Steep Terrain Erosion},
  author    = {Jain, Aryamaan and Benes, Bedrich and Cordonnier, Guillaume},
  journal   = {ACM Transactions on Graphics},
  volume    = {43},
  number    = {4},
  year      = {2024},
  month     = {July},
  articleno = {58},
  numpages  = {11},
  doi       = {10.1145/3658213},
  url       = {https://doi.org/10.1145/3658213}
}

```

---

## Acknowledgements

This project was sponsored by the Agence Nationale de la Recherche project Invterra ANR-22-CE33-0012-01 and research and software donations from Adobe Inc. This project was also sponsored by USDA NIFA, Award #2023-68012-38992 grant "Promoting Economic Resilience and Sustainability of the Eastern U.S. Forests" to Benes. This work is based upon efforts supported by the EFFICACI grant, #NR233A750004G044, under USDA NRCS to Benes. The views and conclusions contained herein are those of the authors and should not be interpreted as representing the official policies, either expressed or implied, of the U.S. Government or NRCS. The U.S. Government is authorized to reproduce and distribute reprints for governmental purposes, notwithstanding any copyright annotation therein.

