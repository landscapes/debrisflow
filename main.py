import os
import numpy as np
import torch
from simulation.Simulation import Simulation

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
torch.set_grad_enabled(False)
if device == 'cpu':
    print('Running simulation on the CPU.\n')
else:
    print('Running simulation on the GPU.\n')

# Load initial terrain
print('Loading initial terrain data...')
simulation = Simulation(np.load('data/init.npy'), dx=8, seed=42)
print('Initial terrain data loaded successfully.\n')

# Run simulation
print('Starting the simulation...')
simulation.run(
    niter=3000,           # Number of iteration of simulation
    dt=128,               # Timestep
    k_uplift=0.0006,      # Uplift rate
    k_rain=0.00049,       # Precipitation constant
    ke_spl=0.0007,        # Fluvial erosion
    kd_spl=1.0,           # Fluvial deposition
    spl_mn=0.4,           # Fluvial exponent (m/n ratio)
    ke_df=0.001,          # Debris-flow erosion
    kt_df=0.3,            # Debris-flow threshold
    ks_df=0.005,          # Thermal erosion
    slope_df=0.57,        # Thermal critical angle
    debris_mn=0.08,       # Debris-flow exponent (alpha/beta ratio)
    debris_mnt=0.25,      # Debris-flow threshold exponent (alpha_t/beta_t ratio)
    ke_hs=0.04,           # Hillslope constant
    lake_flag=1,          # Process lake toggle
)
print('Simulation completed successfully.\n')

# Save output
output_dir = 'data/output/'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
    print(f'Folder "{output_dir}" created.')

print('Saving simulation output data...')
np.save(output_dir + 'surface.npy' , (simulation.dem + simulation.lake_surface).cpu().detach().numpy().squeeze())
np.save(output_dir + 'drain.npy'   , simulation.drainarea.cpu().detach().numpy().squeeze())
np.save(output_dir + 'lake.npy'    , torch.where(simulation.lake_surface>0.0, 1.0, 0.0).cpu().detach().numpy().squeeze())
np.save(output_dir + 'bedrock.npy' , simulation.layer_b.cpu().detach().numpy().squeeze())
np.save(output_dir + 'debris.npy'  , simulation.layer_d.cpu().detach().numpy().squeeze())
np.save(output_dir + 'sediment.npy', simulation.layer_s.cpu().detach().numpy().squeeze())
np.save(output_dir + 'regolith.npy', simulation.layer_r.cpu().detach().numpy().squeeze())
print('All output data saved successfully.')
